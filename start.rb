#!/usr/bin/env ruby
# parser
require 'rubygems'
require 'nokogiri'
require 'open-uri'

url = "http://www.cocktaildb.com/recipe_detail?id="


File.open("drink.csv", "w+") do |f|

  f.puts "drink_name;ingredient;ingredient_key;amount"

  MAX = 4758
  MAX.times do |i|
    drink_url = "#{url}#{i+1}"
    begin
      page = Nokogiri::HTML(open(drink_url))
      drink = page.css("#wellTitle h2").text
      p "#{drink} (#{i+1} / #{MAX}) "
      ingd = page.css(".recipeMeasure")
      ingd.each do |elm|
        elm.css("a[href]").each do |ingd_type|
          txt = ingd_type.text
          amounts = ingd.text
          m = amounts.match /(?<centis>(\d+(\.\d+)? cl))/i
          centis = m ? m['centis'] : "<unknown>"

          f.puts "#{drink};#{txt};#{elm.text};#{centis}"
        end
      end
    rescue
      p "Failed to read drink #{i+1}"
    end
  end
   # ... process the file
end
